This repository provides the source code for a fast parallel in-place merge algorithm, which is related to the paper "On the improvement of the in-place merge algorithm parallelization" (https://hal.inria.fr/hal-02613668).

The content of the directory is the following:
- LICENSE
- main.cpp : the source code of the implementation (including the tests and benchmarks)
- README.md : this file
- tocompile.sh : a simple command to compile the main.cpp file
- torun.sh : the command to run the benchmark
- results/ : the results shown in the paper

## Compile

```bash
$ cat tocompile.sh 
g++ -DNDEBUG -std=c++11 -O3 -mtune=native -march=native main.cpp -o test.exe -fopenmp
```

## To execute

The binary accept a option.

```bash
$ ./test.exe 
[Help] ./test.exe [-reorder;-merge;-time;-find]
```

`-reorder`, `-merge` will run some tests to validate the results.
`-find` will run some tests to validate the results in finding the pivots.
`-time` will run the benchmarks and it is advised to execute them while binding the thread.

