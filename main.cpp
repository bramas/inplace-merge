////////////////////////////////////////////////////////////////
// Author Berenger Bramas
// This code proposes a in place merge of two arrays
// and its corresponding merge sort.
// Please refer to the paper to know more
// Licence is MIT
////////////////////////////////////////////////////////////////
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <sys/time.h>
#include <omp.h>
#include <utility>
#include <algorithm>
#include <cassert>
#include <array>
#include <memory>


// Uncomment the next line to get more output
//#define VERBOSE(V) V;
#ifndef VERBOSE
#define VERBOSE(V)
#endif

////////////////////////////////////////////////////////////////
// Test functions
////////////////////////////////////////////////////////////////

inline double GetTime(){
    return omp_get_wtime();
}

// test if an array is sorted
inline bool isSorted(const int array[], const int size, int* positionUnsorted = nullptr){
    for(int idx = 1 ; idx < size ; ++idx){
        if(array[idx-1] > array[idx]){
            if(positionUnsorted){
                *positionUnsorted = idx;
            }
            return false;
        }
    }
    return true;
}

// print an array [index] = value
inline void print(const int array[], const int size){
    for(int idx = 0 ; idx < size ; ++idx){
        printf("[%d] = %d, ", idx, array[idx]);
    }
    printf("\n");
}

// increment randomly some values in the array
inline void insertIncRandom(int array[], const int size, const int starValue = 0){
    if(size){
        array[0] = starValue + rand()/(RAND_MAX/5);
        for(int idx = 1 ; idx < size ; ++idx){
            array[idx] = (rand()/(RAND_MAX/5)) + array[idx-1];
        }
    }
}

inline void insertRandom(int array[], const int size){
    for(int idx = 0 ; idx < size ; ++idx){
        array[idx] = int(size * double(rand())/RAND_MAX);
    }
}

template <int NbValues>
struct BlockValue{
    int values[NbValues];
    int reference;

    operator int&(){
        return reference;
    }
    
    inline bool operator<=(const BlockValue& inOther) const{
        return reference <= inOther.reference;
    }
    
    inline bool operator<(const BlockValue& inOther) const{
        return reference < inOther.reference;
    }
    
    inline bool operator==(const BlockValue& inOther) const{
        return reference == inOther.reference;
    }
};

// test if an array is sorted
template <int NbValues>
inline bool isSorted(const BlockValue<NbValues> array[], const int size, int* positionUnsorted = nullptr){
    for(int idx = 1 ; idx < size ; ++idx){
        if(!(array[idx-1].reference <= array[idx].reference)){
            if(positionUnsorted){
                *positionUnsorted = idx;
            }
            return false;
        }
    }
    return true;
}

// print an array [index] = value
template <int NbValues>
inline void print(const BlockValue<NbValues> array[], const int size){
    for(int idx = 0 ; idx < size ; ++idx){
        printf("[%d] = %d, ", idx, array[idx].reference);
    }
    printf("\n");
}

// increment randomly some values in the array
template <int NbValues>
inline void insertIncRandom(BlockValue<NbValues> array[], const int size, const int starValue = 0){
    if(size){
        array[0].reference = starValue + rand()/(RAND_MAX/5);
        for(int idxVal = 0 ; idxVal < NbValues ; ++idxVal){
            array[0].values[idxVal] = idxVal;
        }
            
        for(int idx = 1 ; idx < size ; ++idx){
            array[idx].reference = (rand()/(RAND_MAX/5)) + array[idx-1].reference;
            for(int idxVal = 0 ; idxVal < NbValues ; ++idxVal){
                array[idx].values[idxVal] = idxVal;
            }
        }
    }
}

template <int NbValues>
inline void insertRandom(BlockValue<NbValues> array[], const int size){
    for(int idx = 0 ; idx < size ; ++idx){
        array[idx].reference = int(size * double(rand())/RAND_MAX);
        for(int idxVal = 0 ; idxVal < NbValues ; ++idxVal){
            array[idx].values[idxVal] = idxVal;
        }
    }
}

////////////////////////////////////////////////////////////////
// Utils functions
////////////////////////////////////////////////////////////////


// get the max of two values
inline int max(const int firstValue, const int secondeValue){
    return firstValue > secondeValue ? firstValue : secondeValue;
}

// get the PPCM res = v1*c1 = v2*c2
inline int ppcm(long int value1, long int value2, int*const coef1, int*const coef2){
    (*coef1) = 1;
    (*coef2) = 1;
    while( value1 * (*coef1) != value2 * (*coef2) ){
        if(value1 * (*coef1) < value2 * (*coef2)){
            ++(*coef1);
        }
        else{
            ++(*coef2);
        }
    }
    return value1 * (*coef1);
}

////////////////////////////////////////////////////////////////
// Reorder functions
////////////////////////////////////////////////////////////////

// Reorder an array in place with no extra moves :
// if we have [0 1 2 3 4 5 ; A B C]
// it creates [A B C ; 0 1 2 3 4 5]
template <class NumType>
void reorderRolling(NumType array[], const int lengthLeftPart, const int totalSize){
    const int lengthRightPart = totalSize - lengthLeftPart;
    // if one of the partitions has 0 size
    if(lengthLeftPart == 0 || lengthRightPart == 0){
        // do nothing
        return;
    }
    // if partitions have the same size
    else if(lengthLeftPart == lengthRightPart){
        for(int idx = 0 ; idx < lengthLeftPart ; ++idx){
            std::swap(array[idx], array[idx + lengthLeftPart]);
        }
        return;
    }
    else {
        // Otherwise exchange values of the partition
        int coefLeft  = 0;
        int coefRight = 0;
        // get the ppcm
        const int ppc = ppcm(lengthLeftPart, lengthRightPart, &coefLeft, &coefRight);
        // this is "t" in the attached repport
        const int end = lengthLeftPart / coefRight;
        // we need to do end cycle
        for(int idx = 0 ; idx < end; idx += 1){
            // store the first value
            NumType value = std::move(array[idx]);
            // keep track of the first index to know when the cycle is over
            int idxToMove = idx;
            do{
                // While we have to do move to right
                while(idxToMove + lengthRightPart < totalSize){
                    idxToMove += lengthRightPart;
                    std::swap(array[idxToMove], value);
                }
                // while we have to do move to the left
                while(0 <= idxToMove - lengthLeftPart){
                    idxToMove -= lengthLeftPart;
                    std::swap(array[idxToMove], value);
                }
                // the cycle is over when we go back to the first index
            }while(idxToMove != idx);
        }
        return;
    }
}

// Reorder an array in place with extra moves :
// if we have [0 1 2 3 4 5 ; A B C]
// it creates [A B C ; 0 1 2 3 4 5]
template <class NumType>
inline void reorderShifting(NumType array[], const int lengthLeftPart, const int totalSize){
    const int lengthRightPart = totalSize - lengthLeftPart;
    // if one of the size is zero just return
    if(lengthLeftPart == 0 || lengthRightPart == 0){
        // do nothing
        return;
    }

    // size of the partitions at first iteration
    int workingLeftLength  = lengthLeftPart;
    int workingRightLength = lengthRightPart;
    // while the partitions have different sizes and none of them are null
    while(workingLeftLength != workingRightLength && workingLeftLength && workingRightLength){
        // if the left partition is the smallest
        if(workingLeftLength < workingRightLength){
            // move the left parition in the correct place
            for(int idx = 0 ; idx < workingLeftLength ; ++idx){
                std::swap(array[idx], array[idx + workingRightLength]);
            }
            // the new left partition is now the values that have been swaped
            workingRightLength = workingRightLength - workingLeftLength;
            //workingLeftLength = workingLeftLength;
        }
        // if right partition is the smallest
        else{
            // move the right partition in the correct place
            for(int idx = 0 ; idx < workingRightLength ; ++idx){
                std::swap(array[idx], array[idx + workingLeftLength]);
            }
            // shift the pointer to skip the correct values
            array = (array + workingRightLength);
            // the new left partition is the previous right minus the swaped values
            //workingRightLength = workingRightLength;
            workingLeftLength  = workingLeftLength - workingRightLength;
        }
    }
    // if partitions have the same size
    for(int idx = 0 ; idx < workingLeftLength ; ++idx){
        std::swap(array[idx], array[idx + workingLeftLength]);
    }
}

////////////////////////////////////////////////////////////////
// Merge functions
////////////////////////////////////////////////////////////////

template <class NumType, bool useReorderRolling>
inline void mergeInPlace(NumType array[], const int sizeArray, int centerPosition ){
    // Already in good shape
    if(centerPosition == 0 || centerPosition == sizeArray || array[centerPosition-1] <= array[centerPosition]){
        return;
    }
    
    if(sizeArray/2 < centerPosition){
        if(useReorderRolling){
            reorderRolling(array, centerPosition, sizeArray);
        }
        else{
            reorderShifting(array, centerPosition, sizeArray);
        }
        centerPosition = sizeArray - centerPosition;
    }
    
    int idxLeft   = 0;
    int idxRight  = centerPosition;
    
    while(idxLeft < idxRight &&  idxRight < sizeArray){
        idxLeft = (std::upper_bound(&array[idxLeft], &array[idxRight], array[idxRight] ) - array);
            
        if(idxLeft != idxRight){
            int offsetPosition = idxRight;
            idxRight = (std::upper_bound(&array[idxRight], &array[sizeArray], array[idxLeft] ) - array);
                    
            if(useReorderRolling){
                reorderRolling(array + idxLeft, offsetPosition - idxLeft, idxRight - idxLeft);
            }
            else{
                reorderShifting(array + idxLeft, offsetPosition - idxLeft, idxRight - idxLeft);
            }
        }
    }
}


template <class NumType>
void FindMedian(NumType array[],  int centerPosition, const int sizeArray,
                            int* middleA, int* middleB){                            
    if(centerPosition == 0 || centerPosition == sizeArray || array[centerPosition-1] <= array[centerPosition]){
        *middleA = centerPosition;
        *middleB = 0;
        return;
    }
    if(!(array[0] <= array[sizeArray-1])){
        *middleA = 0;
        *middleB = sizeArray-centerPosition;
    }
    
    int leftStart = 0;
    int leftLimite = centerPosition;        
    int leftPivot = (leftLimite-leftStart)/2 + leftStart;
    
    int rightStart = centerPosition;
    int rightLimte = sizeArray;
    int rightPivot = (rightLimte-rightStart)/2 + rightStart;
               
    while(leftStart < leftLimite && rightStart < rightLimte
            && !(array[leftPivot] == array[rightPivot])){
        assert( leftPivot <  leftLimite);  
        assert( rightPivot <  rightLimte);
        assert( leftPivot <  centerPosition);  
        assert( rightPivot <  sizeArray);
        
        const int A0 = leftPivot-0;
        const int A1 = centerPosition-leftPivot;
        const int B0 = rightPivot-centerPosition;
        const int B1 = sizeArray-rightPivot;
    
        if(array[leftPivot] < array[rightPivot]){                        
            if(A0+B0 < A1+B1){
                leftStart = leftPivot+1;
                leftPivot = (leftLimite-leftStart)/2 + leftStart;
            }
            else{
                rightLimte = rightPivot;
                rightPivot = (rightLimte-rightStart)/2 + rightStart;                   
            }
        }
        else{
            if(A0+B0 < A1+B1){
                rightStart = rightPivot+1;
                rightPivot = (rightLimte-rightStart)/2 + rightStart;    
            }
            else{     
                leftLimite = leftPivot;
                leftPivot = (leftLimite-leftStart)/2 + leftStart;         
            }
        }
    }
    
    *middleA = leftPivot;
    *middleB = rightPivot-centerPosition;
}


template <class NumType, bool useReorderRolling>
inline void parallelMergeInPlaceCore(NumType array[], int currentStart, int currentMiddle, int currentEnd,
                                    int level, const int depthLimite){
          
    assert(0 <= currentStart);
    assert(currentStart <= currentMiddle);
    assert(currentMiddle <= currentEnd);
  
    if(currentStart != currentMiddle && currentMiddle != currentEnd){
        while(level != depthLimite && (currentEnd-currentStart) > 256){        
            int middleA = 0;
            int middleB = 0;
           
            FindMedian(array + currentStart,  currentMiddle - currentStart, currentEnd-currentStart,
                        &middleA, &middleB);
                                  
            const int sizeRestA = currentMiddle-currentStart-middleA;
            const int sizeRestB = currentEnd-currentMiddle-middleB;
                        
            if(useReorderRolling){
                reorderRolling(array + middleA + currentStart, sizeRestA, middleB+sizeRestA);
            }
            else{
                reorderShifting(array + middleA + currentStart, sizeRestA, middleB+sizeRestA);
            }
            
            #pragma omp task default(none) firstprivate(array,level,currentStart,currentMiddle,currentEnd,middleA,middleB)
            parallelMergeInPlaceCore<NumType,useReorderRolling>(array,
                                     currentStart+middleA+middleB,
                                     currentStart+middleA+middleB+sizeRestA,
                                     currentEnd,
                                     level+1, depthLimite);
            
            currentEnd = currentStart+middleA+middleB;
            currentMiddle = currentStart+middleA;       
        
            assert(0 <= currentStart);
            assert(currentStart <= currentMiddle);
            assert(currentMiddle <= currentEnd);
              
            level += 1;
        }
        

        std::inplace_merge(array + currentStart, array+currentMiddle, array+currentEnd); 
        
        #pragma omp taskwait

        ///#pragma omp critical(stdout)
        //printf("Thread %d do from %d middle %d to %d - s1 %d s2 %d\n", omp_get_thread_num(), currentStart, currentMiddle, currentEnd,
       //    currentMiddle-currentStart, currentEnd - currentMiddle);
    }
}

template <class NumType, bool useReorderRolling>
inline void parallelMergeInPlace(NumType array[], const int sizeArray, int centerPosition ){
    // Already in good shape
    if(centerPosition == 0 || centerPosition == sizeArray || array[centerPosition-1] <= array[centerPosition]){
        return;
    }
        
    #pragma omp parallel
    #pragma omp master
    {
        const int numThreads = omp_get_num_threads();
        //assert((numThreads & numThreads-1) == 0):
        const int depthLimite = ffs(numThreads) - 1;

        parallelMergeInPlaceCore<NumType,useReorderRolling>(array, 0, centerPosition, sizeArray, 0, depthLimite);
    }
}


////////////////////////////////////////////////////////////////
// Multi version
////////////////////////////////////////////////////////////////


struct Move{
    int begin;
    int end;
    int shift;
};


template <class NumType>
void reorderRollingMulti(NumType array[], const Move moves[], const int nbMoves, const int arrayLimit){
    if(nbMoves == 0){
        return;
    }

    int maxValue = std::numeric_limits<int>::min();
    int minValue = std::numeric_limits<int>::max();

    for(int idxVal = moves[0].begin ; idxVal < arrayLimit ; ++idxVal){
        maxValue = std::max(maxValue, int(array[idxVal]));
        minValue = std::min(minValue, int(array[idxVal]));
    }

    const int marker = maxValue - minValue + 1;

    for(int idxVal = moves[0].begin ; idxVal < arrayLimit ; ++idxVal){
        if(int(array[idxVal]) <= maxValue){
            // store the first value
            NumType value = std::move(array[idxVal]);
            // keep track of the first index to know when the cycle is over
            int idxToMove = idxVal;
            do {
                value += marker;
                const Move& currentMove = *std::upper_bound(&moves[0], &moves[nbMoves], idxToMove,
                        [](const int idxToTest, const Move& move){
                    return (idxToTest < move.end);
                });
                assert(&currentMove != &moves[nbMoves]);
                assert(currentMove.begin <= idxToMove && idxToMove < currentMove.end);
                const int nextMove = idxToMove + currentMove.shift;
                assert(int(array[nextMove]) <= maxValue || nextMove == idxVal);
                std::swap(array[nextMove], value);
                idxToMove = nextMove;
            } while(idxToMove != idxVal);
        }
    }

    for(int idxVal = moves[0].begin ; idxVal < arrayLimit ; ++idxVal){
        assert(int(array[idxVal]) > maxValue);
        array[idxVal] -= marker;
    }

    return;
}


template <class NumType>
void FindMedianMulti(const NumType A[], const int sizeA, const NumType B[], const int sizeB,
                 int* middleA, int* middleB){
    if(sizeA == 0 || sizeB == 0 || A[sizeA-1] <= B[0]){
        *middleA = sizeA;
        *middleB = 0;
        return;
    }
    if(!(A[0] <= B[sizeB-1])){
        *middleA = 0;
        *middleB = sizeB;
    }

    int leftStart = 0;
    int leftLimite = sizeA;
    int leftPivot = (leftLimite-leftStart)/2 + leftStart;

    int rightStart = 0;
    int rightLimte = sizeB;
    int rightPivot = (rightLimte-rightStart)/2 + rightStart;

    while(leftStart < leftLimite && rightStart < rightLimte
            && !(A[leftPivot] == B[rightPivot])){
        assert( leftPivot <  leftLimite);
        assert( rightPivot <  rightLimte);
        assert( leftPivot <  sizeA);
        assert( rightPivot <  sizeB);

        const int A0 = leftPivot-0;
        const int A1 = sizeA-leftPivot;
        const int B0 = rightPivot-0;
        const int B1 = sizeB-rightPivot;

        if(A[leftPivot] < B[rightPivot]){
            if(A0+B0 < A1+B1){
                leftStart = leftPivot+1;
                leftPivot = (leftLimite-leftStart)/2 + leftStart;
            }
            else{
                rightLimte = rightPivot;
                rightPivot = (rightLimte-rightStart)/2 + rightStart;
            }
        }
        else{
            if(A0+B0 < A1+B1){
                rightStart = rightPivot+1;
                rightPivot = (rightLimte-rightStart)/2 + rightStart;
            }
            else{
                leftLimite = leftPivot;
                leftPivot = (leftLimite-leftStart)/2 + leftStart;
            }
        }
    }

//    if(A[leftPivot] == B[rightPivot]){
//        const int Alower = std::distance(A, std::lower_bound(A, A + sizeA, A[leftPivot]));
//        const int Aupper = std::distance(A, std::upper_bound(A, A + sizeA, A[leftPivot]));
//        const int Blower = std::distance(B, std::lower_bound(B, B + sizeB, B[rightPivot]));
//        const int Bupper = std::distance(B, std::upper_bound(B, B + sizeB, B[rightPivot]));

//        const int minA0B0 = Alower + Blower;
//        const int maxA0B0 = Aupper + Bupper;
//        const int ideal = (sizeA+sizeB)/2;

//        if(ideal < minA0B0){
//            leftPivot = Alower;
//            rightPivot = Blower;
//        }
//        else if(maxA0B0 < ideal){
//            leftPivot = Aupper;
//            rightPivot = Bupper;
//        }
//        else{
//            leftPivot = std::min(Alower + ideal-minA0B0, Aupper);
//            rightPivot = std::min(Blower + ideal-minA0B0-(leftPivot-Alower), Bupper);
//        }
//    }

    *middleA = leftPivot;
    *middleB = rightPivot;

    assert(sizeA && sizeB && ((*middleA) == 0 || (*middleB) == sizeB || A[(*middleA)-1] <= B[(*middleB)]));
    assert(sizeA && sizeB && ((*middleB) == 0 || (*middleA) == sizeA || B[(*middleB)-1] <= A[(*middleA)]));
}

template <class NumType>
void FindMedianSelimSantoro(const NumType A[], const int n, const NumType B[], const int m,
                 int* middleA, int* middleB){
    int low_A = 1;
    int low_B = 1;
    int high_A = n;
    int high_B = m;
    int r_A = n;
    int r_B = m;

    int u = n/2;
    int v = m/2;

    while(r_A > 1 && r_B > 1){
        if( A[u-1] >= B[v-1] ){
            r_A = u - low_A + 1;
            r_B = high_B - v;
            high_A = u;
            low_B = v + 1;
        }
        else{
            r_A = high_A - u;
            r_B = v - low_B + 1;
            low_A = u + 1;
            high_B = v;
        }
        u = low_A + (high_A - low_A - 1)/2;
        v = low_B + (high_B - low_B - 1)/2;
    }

    // This is unclear how they want to make it
    if(r_A <= 1 && r_B <= 1){
        *middleA = (u-1+1)<0?0:(u-1+1)>=n?n-1:(u-1+1);
        //*middleB = (v-1+1)<0?0:(v-1+1)>=m?m-1:(v-1+1);
        *middleB = std::distance(B, std::lower_bound(B, B + m, A[*middleA]));
    }
    else if(r_A <= 1){
        *middleA = (u-1+1)<0?0:(u-1+1)>=n?n-1:(u-1+1);
        *middleB = std::distance(B, std::lower_bound(B, B + m, A[*middleA]));
    }
    else{
        *middleB = (v-1+1)<0?0:(v-1+1)>=m?m-1:(v-1+1);
        *middleA = std::distance(A, std::lower_bound(A, A + n, B[*middleB]));
    }

    assert(m && n && ((*middleA) == 0 || A[(*middleA)-1] <= B[(*middleB)]));
    assert(m && n && ((*middleB) == 0 || B[(*middleB)-1] <= A[(*middleA)]));
}


template <class NumType>
inline void parallelMergeInPlaceMulti(NumType array[], const int sizeArray, int centerPosition ){
    // Already in good shape
    if(centerPosition == 0 || centerPosition == sizeArray || array[centerPosition-1] <= array[centerPosition]){
        return;
    }

    #pragma omp parallel
    #pragma omp master
    {
        //assert((numThreads & numThreads-1) == 0):
        const int depthLimite = ffs(omp_get_num_threads()) - 1;
        const int numThreads = (1 << depthLimite);

        std::unique_ptr<std::array<int,4>[]> srcMove(new std::array<int,4>[numThreads]);
        std::unique_ptr<std::array<int,4>[]> dstMove(new std::array<int,4>[numThreads]);

        srcMove[0][0] = 0;
        srcMove[0][1] = centerPosition;
        srcMove[0][2] = centerPosition;
        srcMove[0][3] = sizeArray;

        for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
            const int nbSubParts = (1 << idxDepth);

            for(int idxSubPart = 0 ; idxSubPart < nbSubParts ; ++idxSubPart){
                int middleA = -1, middleB = -1;
                FindMedianMulti(&array[srcMove[idxSubPart][0]], srcMove[idxSubPart][1]-srcMove[idxSubPart][0],
                            &array[srcMove[idxSubPart][2]], srcMove[idxSubPart][3]-srcMove[idxSubPart][2],
                            &middleA, &middleB);

                dstMove[idxSubPart*2][0] = srcMove[idxSubPart][0];
                dstMove[idxSubPart*2][1] = srcMove[idxSubPart][0]+middleA;
                dstMove[idxSubPart*2][2] = srcMove[idxSubPart][2];
                dstMove[idxSubPart*2][3] = srcMove[idxSubPart][2]+middleB;

                dstMove[idxSubPart*2+1][0] = srcMove[idxSubPart][0]+middleA;
                dstMove[idxSubPart*2+1][1] = srcMove[idxSubPart][1];
                dstMove[idxSubPart*2+1][2] = srcMove[idxSubPart][2]+middleB;
                dstMove[idxSubPart*2+1][3] = srcMove[idxSubPart][3];
            }

            std::swap(srcMove, dstMove);
        }

        std::unique_ptr<Move[]> moves(new Move[2*numThreads]);
        {
            const int nbSubParts = (1 << depthLimite);
            assert(numThreads == nbSubParts );
            int currentPos = 0;

            for(int idxSubPart = 0 ; idxSubPart < nbSubParts ; ++idxSubPart){
                moves[idxSubPart*2].begin = srcMove[idxSubPart][0];
                moves[idxSubPart*2].end = srcMove[idxSubPart][1];
                moves[idxSubPart*2].shift = currentPos-srcMove[idxSubPart][0];
                currentPos += moves[idxSubPart*2].end - moves[idxSubPart*2].begin;

                moves[idxSubPart*2+1].begin = srcMove[idxSubPart][2];
                moves[idxSubPart*2+1].end = srcMove[idxSubPart][3];
                moves[idxSubPart*2+1].shift = currentPos-srcMove[idxSubPart][2];
                currentPos += moves[idxSubPart*2+1].end - moves[idxSubPart*2+1].begin;
            }

            assert(moves[0].shift == 0);
            assert(moves[2*numThreads-1].shift == 0);
        }

        std::sort(&moves[0], &moves[2*numThreads], [](const Move& mv1, const Move& mv2){
            return mv1.begin < mv2.begin;
        });

        int nonEmptyMove = 0;
        for(int idxMove = 0 ; idxMove < 2*numThreads ; ++idxMove){
            if(moves[idxMove].begin != moves[idxMove].end
                    && moves[idxMove].shift != 0){
                if(nonEmptyMove != idxMove){
                    moves[nonEmptyMove] = moves[idxMove];
                }
                nonEmptyMove += 1;
            }
        }

        reorderRollingMulti(array, &moves[0], nonEmptyMove,
                            moves[nonEmptyMove-1].end);


        {
            int currentPos = 0;
            for(int idxThread = 0 ; idxThread < numThreads ; ++idxThread){
                const int currentMiddle = currentPos + (srcMove[idxThread][1] - srcMove[idxThread][0]);
                const int currentEnd = currentMiddle + (srcMove[idxThread][3] - srcMove[idxThread][2]);
                #pragma omp task default(none) shared(array) firstprivate(currentPos, currentMiddle, currentEnd)
                std::inplace_merge(array + currentPos, array+currentMiddle, array+currentEnd);
                currentPos = currentEnd;
            }
            assert(currentPos == sizeArray);

            #pragma omp taskwait
        }
    }
}


////////////////////////////////////////////////////////////////
// To compare FindMedian with greedy version
////////////////////////////////////////////////////////////////



template <class NumType>
void FindMedianGreedy(const NumType A[], const int sizeA, const NumType B[], const int sizeB,
                 int* middleA, int* middleB){
    if(sizeA == 0 || sizeB == 0 || A[sizeA-1] <= B[0]){
        *middleA = sizeA;
        *middleB = 0;
        return;
    }
    if(!(A[0] <= B[sizeB-1])){
        *middleA = 0;
        *middleB = sizeB;
    }

    int bestDiff = sizeA + sizeB;
    int bestLeftPivot = -1;
    int bestRightPivot = -1;

    for(int idxA = 0 ; idxA < sizeA ; ++idxA){
        {
            const int rightPivot = std::distance(B,
                                                 std::upper_bound(B, B + sizeB, A[idxA]));

            const int A0 = idxA;
            const int A1 = sizeA-idxA;
            const int B0 = rightPivot-0;
            const int B1 = sizeB-rightPivot;
            const int currentDiff = std::abs((A0+B0) - (A1+B1));

            if(currentDiff < bestDiff){
                bestDiff = currentDiff;
                bestLeftPivot = idxA;
                bestRightPivot = rightPivot;
            }
        }
        {
            const int rightPivot = std::distance(B,
                                                 std::lower_bound(B, B + sizeB, A[idxA]));

            const int A0 = idxA;
            const int A1 = sizeA-idxA;
            const int B0 = rightPivot-0;
            const int B1 = sizeB-rightPivot;
            const int currentDiff = std::abs((A0+B0) - (A1+B1));

            if(currentDiff < bestDiff){
                bestDiff = currentDiff;
                bestLeftPivot = idxA;
                bestRightPivot = rightPivot;
            }
        }
    }

    *middleA = bestLeftPivot;
    *middleB = bestRightPivot;
}

void testFindMiddle(){
    const int MaxSize = 1<<22; //1<<19;

    const int depthLimite = ffs(omp_get_max_threads()) - 1;
    const int numThreads = (1 << depthLimite);

    printf("depthLimite = %d\n", depthLimite);
    printf("numThreads = %d\n", numThreads);

    std::unique_ptr<double[]> diffVsBest(new double[24*depthLimite]());
    std::unique_ptr<double[]> diffVsBestSelimSantoro(new double[24*depthLimite]());

    for(int Size = 4 ; Size <= MaxSize ; Size *= 2){
        int*const array = new int[Size];

        for(int idxMiddle = 0 ; idxMiddle < 3 ; ++idxMiddle){
            const int middle = (idxMiddle+1)*(Size/4);
            {
                srand(0);
                insertIncRandom(array, middle);
                insertIncRandom(array+middle, Size-middle);

                {
                    std::unique_ptr<std::array<int,4>[]> srcMove(new std::array<int,4>[numThreads]);
                    std::unique_ptr<std::array<int,4>[]> dstMove(new std::array<int,4>[numThreads]);

                    srcMove[0][0] = 0;
                    srcMove[0][1] = middle;
                    srcMove[0][2] = middle;
                    srcMove[0][3] = Size;

                    std::unique_ptr<std::array<int,4>[]> srcMoveGreedy(new std::array<int,4>[numThreads]);
                    std::unique_ptr<std::array<int,4>[]> dstMoveGreedy(new std::array<int,4>[numThreads]);

                    srcMoveGreedy[0][0] = 0;
                    srcMoveGreedy[0][1] = middle;
                    srcMoveGreedy[0][2] = middle;
                    srcMoveGreedy[0][3] = Size;

                    std::unique_ptr<std::array<int,4>[]> srcMoveSelimSantoro(new std::array<int,4>[numThreads]);
                    std::unique_ptr<std::array<int,4>[]> dstMoveSelimSantoro(new std::array<int,4>[numThreads]);

                    srcMoveSelimSantoro[0][0] = 0;
                    srcMoveSelimSantoro[0][1] = middle;
                    srcMoveSelimSantoro[0][2] = middle;
                    srcMoveSelimSantoro[0][3] = Size;

                    for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
                        const int nbSubParts = (1 << idxDepth);

                        for(int idxSubPart = 0 ; idxSubPart < nbSubParts ; ++idxSubPart){
                            {
                                if(srcMove[idxSubPart][1]-srcMove[idxSubPart][0]
                                        && srcMove[idxSubPart][3]-srcMove[idxSubPart][2]){
                                    int middleA = -1, middleB = -1;
                                    FindMedianMulti(&array[srcMove[idxSubPart][0]], srcMove[idxSubPart][1]-srcMove[idxSubPart][0],
                                                &array[srcMove[idxSubPart][2]], srcMove[idxSubPart][3]-srcMove[idxSubPart][2],
                                                &middleA, &middleB);

                                    dstMove[idxSubPart*2][0] = srcMove[idxSubPart][0];
                                    dstMove[idxSubPart*2][1] = srcMove[idxSubPart][0]+middleA;
                                    dstMove[idxSubPart*2][2] = srcMove[idxSubPart][2];
                                    dstMove[idxSubPart*2][3] = srcMove[idxSubPart][2]+middleB;

                                    dstMove[idxSubPart*2+1][0] = srcMove[idxSubPart][0]+middleA;
                                    dstMove[idxSubPart*2+1][1] = srcMove[idxSubPart][1];
                                    dstMove[idxSubPart*2+1][2] = srcMove[idxSubPart][2]+middleB;
                                    dstMove[idxSubPart*2+1][3] = srcMove[idxSubPart][3];
                                }
                                else{
                                    dstMove[idxSubPart*2][0] = srcMove[idxSubPart][0];
                                    dstMove[idxSubPart*2][1] = srcMove[idxSubPart][0]+0;
                                    dstMove[idxSubPart*2][2] = srcMove[idxSubPart][2];
                                    dstMove[idxSubPart*2][3] = srcMove[idxSubPart][2]+0;

                                    dstMove[idxSubPart*2+1][0] = srcMove[idxSubPart][0]+0;
                                    dstMove[idxSubPart*2+1][1] = srcMove[idxSubPart][1];
                                    dstMove[idxSubPart*2+1][2] = srcMove[idxSubPart][2]+0;
                                    dstMove[idxSubPart*2+1][3] = srcMove[idxSubPart][3];
                                }
                            }
                            {
                                if(srcMoveGreedy[idxSubPart][1]-srcMoveGreedy[idxSubPart][0]
                                        && srcMoveGreedy[idxSubPart][3]-srcMoveGreedy[idxSubPart][2]){
                                    int middleA = -1, middleB = -1;
                                    FindMedianGreedy(&array[srcMoveGreedy[idxSubPart][0]], srcMoveGreedy[idxSubPart][1]-srcMoveGreedy[idxSubPart][0],
                                                &array[srcMoveGreedy[idxSubPart][2]], srcMoveGreedy[idxSubPart][3]-srcMoveGreedy[idxSubPart][2],
                                                &middleA, &middleB);

                                    dstMoveGreedy[idxSubPart*2][0] = srcMoveGreedy[idxSubPart][0];
                                    dstMoveGreedy[idxSubPart*2][1] = srcMoveGreedy[idxSubPart][0]+middleA;
                                    dstMoveGreedy[idxSubPart*2][2] = srcMoveGreedy[idxSubPart][2];
                                    dstMoveGreedy[idxSubPart*2][3] = srcMoveGreedy[idxSubPart][2]+middleB;

                                    dstMoveGreedy[idxSubPart*2+1][0] = srcMoveGreedy[idxSubPart][0]+middleA;
                                    dstMoveGreedy[idxSubPart*2+1][1] = srcMoveGreedy[idxSubPart][1];
                                    dstMoveGreedy[idxSubPart*2+1][2] = srcMoveGreedy[idxSubPart][2]+middleB;
                                    dstMoveGreedy[idxSubPart*2+1][3] = srcMoveGreedy[idxSubPart][3];
                                }
                                else{
                                    dstMoveGreedy[idxSubPart*2][0] = srcMoveGreedy[idxSubPart][0];
                                    dstMoveGreedy[idxSubPart*2][1] = srcMoveGreedy[idxSubPart][0]+0;
                                    dstMoveGreedy[idxSubPart*2][2] = srcMoveGreedy[idxSubPart][2];
                                    dstMoveGreedy[idxSubPart*2][3] = srcMoveGreedy[idxSubPart][2]+0;

                                    dstMoveGreedy[idxSubPart*2+1][0] = srcMoveGreedy[idxSubPart][0]+0;
                                    dstMoveGreedy[idxSubPart*2+1][1] = srcMoveGreedy[idxSubPart][1];
                                    dstMoveGreedy[idxSubPart*2+1][2] = srcMoveGreedy[idxSubPart][2]+0;
                                    dstMoveGreedy[idxSubPart*2+1][3] = srcMoveGreedy[idxSubPart][3];
                                }
                            }
                            {
                                if(srcMoveSelimSantoro[idxSubPart][1]-srcMoveSelimSantoro[idxSubPart][0]
                                        && srcMoveSelimSantoro[idxSubPart][3]-srcMoveSelimSantoro[idxSubPart][2]){
                                    int middleA = -1, middleB = -1;
                                    FindMedianSelimSantoro(&array[srcMoveSelimSantoro[idxSubPart][0]],
                                                srcMoveSelimSantoro[idxSubPart][1]-srcMoveSelimSantoro[idxSubPart][0],
                                                &array[srcMoveSelimSantoro[idxSubPart][2]], srcMoveSelimSantoro[idxSubPart][3]-srcMoveSelimSantoro[idxSubPart][2],
                                                &middleA, &middleB);

                                    dstMoveSelimSantoro[idxSubPart*2][0] = srcMoveSelimSantoro[idxSubPart][0];
                                    dstMoveSelimSantoro[idxSubPart*2][1] = srcMoveSelimSantoro[idxSubPart][0]+middleA;
                                    dstMoveSelimSantoro[idxSubPart*2][2] = srcMoveSelimSantoro[idxSubPart][2];
                                    dstMoveSelimSantoro[idxSubPart*2][3] = srcMoveSelimSantoro[idxSubPart][2]+middleB;

                                    dstMoveSelimSantoro[idxSubPart*2+1][0] = srcMoveSelimSantoro[idxSubPart][0]+middleA;
                                    dstMoveSelimSantoro[idxSubPart*2+1][1] = srcMoveSelimSantoro[idxSubPart][1];
                                    dstMoveSelimSantoro[idxSubPart*2+1][2] = srcMoveSelimSantoro[idxSubPart][2]+middleB;
                                    dstMoveSelimSantoro[idxSubPart*2+1][3] = srcMoveSelimSantoro[idxSubPart][3];
                                }
                                else{
                                    dstMoveSelimSantoro[idxSubPart*2][0] = srcMoveSelimSantoro[idxSubPart][0];
                                    dstMoveSelimSantoro[idxSubPart*2][1] = srcMoveSelimSantoro[idxSubPart][0]+0;
                                    dstMoveSelimSantoro[idxSubPart*2][2] = srcMoveSelimSantoro[idxSubPart][2];
                                    dstMoveSelimSantoro[idxSubPart*2][3] = srcMoveSelimSantoro[idxSubPart][2]+0;

                                    dstMoveSelimSantoro[idxSubPart*2+1][0] = srcMoveSelimSantoro[idxSubPart][0]+0;
                                    dstMoveSelimSantoro[idxSubPart*2+1][1] = srcMoveSelimSantoro[idxSubPart][1];
                                    dstMoveSelimSantoro[idxSubPart*2+1][2] = srcMoveSelimSantoro[idxSubPart][2]+0;
                                    dstMoveSelimSantoro[idxSubPart*2+1][3] = srcMoveSelimSantoro[idxSubPart][3];
                                }
                            }
                        }

                        std::swap(srcMove, dstMove);
                        std::swap(srcMoveGreedy, dstMoveGreedy);
                        std::swap(srcMoveSelimSantoro, dstMoveSelimSantoro);

                        int duration = std::numeric_limits<int>::min();
                        int durationGreedy = std::numeric_limits<int>::min();
                        int durationSelimSantoro = std::numeric_limits<int>::min();

                        for(int idxSubPart = 0 ; idxSubPart < 2*nbSubParts ; ++idxSubPart){
                            duration = std::max(duration, (srcMove[idxSubPart][1] - srcMove[idxSubPart][0]) + (srcMove[idxSubPart][3] - srcMove[idxSubPart][2]));
                            durationGreedy = std::max(durationGreedy, (srcMoveGreedy[idxSubPart][1] - srcMoveGreedy[idxSubPart][0]) + (srcMoveGreedy[idxSubPart][3] - srcMoveGreedy[idxSubPart][2]));
                            durationSelimSantoro = std::max(durationSelimSantoro, (srcMoveSelimSantoro[idxSubPart][1] - srcMoveSelimSantoro[idxSubPart][0]) + (srcMoveSelimSantoro[idxSubPart][3] - srcMoveSelimSantoro[idxSubPart][2]));

                            VERBOSE(printf("Size = %d (%d) -- depth = %d -- interval %d -- v1 %d %d -- v2 %d %d\n",
                                   Size, middle, idxDepth, idxSubPart,
                                   srcMove[idxSubPart][1] - srcMove[idxSubPart][0], srcMove[idxSubPart][3] - srcMove[idxSubPart][2],
                                   srcMoveGreedy[idxSubPart][1] - srcMoveGreedy[idxSubPart][0], srcMoveGreedy[idxSubPart][3] - srcMoveGreedy[idxSubPart][2]));
                        }

                        VERBOSE(printf("duration %d -- durationGreed %d\n",duration, durationGreedy));

                        diffVsBest[ffs(Size)*depthLimite+idxDepth] += (duration - durationGreedy)/double(durationGreedy);

                        diffVsBestSelimSantoro[ffs(Size)*depthLimite+idxDepth] += (durationSelimSantoro - durationGreedy)/double(durationGreedy);
                    }
                }
            }
        }

        delete[] array;
    }


    printf("# Size ");
    for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
        const int nbSubParts = (1 << idxDepth) * 2;
        printf("\tthreads-%d ", nbSubParts);
    }
    for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
        const int nbSubParts = (1 << idxDepth) * 2;
        printf("\tthreads-%d-salim ", nbSubParts);
    }
    printf("\n");

    for(int Size = 2 ; Size <= MaxSize ; Size *= 2){
        printf("%d ", Size);
        for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
            const int nbSubParts = (1 << idxDepth);
            printf("\t%e ", diffVsBest[ffs(Size)*depthLimite+idxDepth]/3.);
        }
        for(int idxDepth = 0 ; idxDepth < depthLimite ; ++idxDepth){
            const int nbSubParts = (1 << idxDepth);
            printf("\t%e ", diffVsBestSelimSantoro[ffs(Size)*depthLimite+idxDepth]/3.);
        }
        printf("\n");
    }
}


////////////////////////////////////////////////////////////////
// Test functions functions
////////////////////////////////////////////////////////////////

// test the reorder function
// it creates two sorted arrays A & B
// with B(end) < A(0)
// So after reordeing the result has to be sorted
template <class NumType, bool useReorderRolling>
void testReorder(){
    // from 4 to 50
    for( int Size = 4 ; Size < 50 ; ++Size){
        NumType*const array = new NumType[Size];
        // Middle from 0 to size
        for(int middle = 0 ; middle < Size ; ++middle){
            printf("[Test] Size %d middle %d\n",Size, middle);

            insertIncRandom(array+middle, Size-middle);
            insertIncRandom(array, middle, array[Size-1]);

            VERBOSE(printf("First part is sorted %s\n", isSorted(array,middle)?"True":"False"));
            VERBOSE(print(array,middle));
            VERBOSE(printf("Second part is sorted %s\n", isSorted(array+middle,Size-middle)?"True":"False"));
            VERBOSE(print(array + middle,Size-middle));

            if(useReorderRolling)
                reorderRolling(array,middle,Size);
            else
                reorderShifting(array,middle,Size);

            VERBOSE(printf("Finaly is sorted %s\n", isSorted(array,Size)?"True":"False"));
            VERBOSE(print(array,Size));
            if( !isSorted(array,Size) ){
                printf("Error, no sorted!\n");
                print(array,Size);
                delete[] array;
                exit(0);
            }
        }

        delete[] array;
    }
}

// Test the merge function by generating two
// random already sorted array so the result
// has to be the union sorted of the inputs
template <class NumType, bool useReorderRolling>
void testMerge(){
    for(int Size = 1 ; Size < 100 ; ++Size){
        NumType*const array = new NumType[Size];
        for(int middle = 0 ; middle <= Size ; ++middle){
            printf("[Test] Size %d middle %d\n",Size, middle);

            insertIncRandom(array, middle);
            insertIncRandom(array+middle, Size-middle);

            VERBOSE(printf("First part is sorted %s\n", isSorted(array,middle)?"True":"False"));
            VERBOSE(printf("Second part is sorted %s\n", isSorted(array+middle,Size-middle)?"True":"False"));

            mergeInPlace<NumType, useReorderRolling>(array,Size,middle);

            VERBOSE(printf("Finaly is sorted %s\n", isSorted(array,Size)?"True":"False"));
            VERBOSE(print(array,Size));
            if( !isSorted(array,Size) ){
                printf("Error, no sorted!\n");
                print(array,Size);
                delete[] array;
                exit(0);
            }
        }
        delete[] array;
    }
}

// Compare big merge direct and undirect

template <class NumType>
void testMergeTime(){
    const int MaxSize = 1<<22; //1<<19;
    const int NbLoops = 50;
    
    const int startThread = 1;
    const int endThread = omp_get_max_threads();

    printf("# Size \tExternal \tinplace_merge");
    
    for(int idxThread = startThread ; idxThread <= endThread ; idxThread *= 2){
        printf("\tparallel-permute-%d \tparallel-shift-%d \tparallel-multi-%d", idxThread, idxThread, idxThread);
    }
    printf("\n");

    for(int Size = 2 ; Size <= MaxSize ; Size *= 2){
        NumType*const array = new NumType[Size];

        double totalExternal = 0;
        double totalInplace = 0;
        double totalMergeParallelPermute[128] = {0};
        double totalMergeParallelShift[128] = {0};
        double totalMergeParallelMulti[128] = {0};

        for(int idxLoop = 0 ; idxLoop < NbLoops ; ++idxLoop){
            for(int idxMiddle = 0 ; idxMiddle < 3 ; ++idxMiddle){
                const int middle = (idxMiddle+1)*(Size/4);
                {
                    srand(0);
                    insertIncRandom(array, middle);
                    insertIncRandom(array+middle, Size-middle);

                    const double begin = GetTime();
                    {
                        NumType*const arrayTemp = new NumType[Size];
                        memcpy(arrayTemp,array,sizeof(NumType)*Size);

                        int idx = 0;
                        int idx1 = 0, idx2 = middle;
                        while(idx1 < middle && idx2 < Size){
                            if(arrayTemp[idx1] < arrayTemp[idx2]) array[idx++] = arrayTemp[idx1++];
                            else array[idx++] = arrayTemp[idx2++];
                        }
                        while(idx1 < middle){
                            array[idx++] = arrayTemp[idx1++];
                        }
                        while(idx2 < Size){
                            array[idx++] = arrayTemp[idx2++];
                        }

                        delete[] arrayTemp;
                    }
                    const double end = GetTime();
                    totalExternal += (end-begin);
                    const bool valide = isSorted(array,Size);
                    VERBOSE(printf("Time taken %lf s (Sorted %s)\n", end-begin, valide?"True":"False"));
                    if(!valide){
                        printf("Exit at loop %d idxMiddle %d size %d external\n", idxLoop, idxMiddle, Size);
                        exit(99);
                    }
                }
                {
                    srand(0);
                    insertIncRandom(array, middle);
                    insertIncRandom(array+middle, Size-middle);

                    const double begin = GetTime();
                    std::inplace_merge(array,array+middle,array+Size);
                    const double end = GetTime();

                    totalInplace += end-begin;
                    const bool valide = isSorted(array,Size);
                    VERBOSE(printf("Time taken %lf s (Sorted %s)\n", end-begin, valide?"True":"False"));
                    if(!valide){
                        printf("Exit at loop %d idxMiddle %d size %d std::inplace_merge\n", idxLoop, idxMiddle, Size);
                        print(array, Size);
                        exit(99);
                    }
                }            
                
                for(int idxThread = startThread ; idxThread <= endThread ; idxThread *= 2){
                    omp_set_num_threads(idxThread);
                    {
                        srand(0);
                        insertIncRandom(array, middle);
                        insertIncRandom(array+middle, Size-middle);

                        const double begin = GetTime();
                        parallelMergeInPlace<NumType, true>(array,Size,middle);
                        const double end = GetTime();

                        totalMergeParallelPermute[idxThread] += end-begin;
                        int position = 0;
                        const bool valide = isSorted(array,Size, &position);
                        VERBOSE(printf("Time taken %lf s (Sorted %s)\n", end-begin, valide?"True":"False"));
                        if(!valide){
                            printf("Exit at loop %d middle %d size %d parallelMergeInPlace<NumType, true> %d\n", idxLoop, middle, Size, position);
                            print(array, Size);
                            exit(99);
                        }
                    }          
                    {
                        srand(0);
                        insertIncRandom(array, middle);
                        insertIncRandom(array+middle, Size-middle);

                        const double begin = GetTime();
                        parallelMergeInPlace<NumType, false>(array,Size,middle);
                        const double end = GetTime();

                        totalMergeParallelShift[idxThread] += end-begin;
                        int position = 0;
                        const bool valide = isSorted(array,Size, &position);
                        VERBOSE(printf("Time taken %lf s (Sorted %s)\n", end-begin, valide?"True":"False"));
                        if(!valide){
                            printf("Exit at loop %d middle %d size %d parallelMergeInPlace<NumType, false> %d\n", idxLoop, middle, Size, position);
                            print(array, Size);
                            exit(99);
                        }
                    }
                    {
                        srand(0);
                        insertIncRandom(array, middle);
                        insertIncRandom(array+middle, Size-middle);

                        const double begin = GetTime();
                        parallelMergeInPlaceMulti<NumType>(array,Size,middle);
                        const double end = GetTime();

                        totalMergeParallelMulti[idxThread] += end-begin;
                        int position = 0;
                        const bool valide = isSorted(array,Size, &position);
                        VERBOSE(printf("Time taken %lf s (Sorted %s)\n", end-begin, valide?"True":"False"));
                        if(!valide){
                            printf("Exit at loop %d middle %d size %d parallelMergeInPlaceMulti<NumType> %d\n", idxLoop, middle, Size, position);
                            print(array, Size);
                            exit(99);
                        }
                    }
                }
            }
        }

        printf("%d \t%e \t%e", Size,
               totalExternal/(3.0*double(NbLoops)), totalInplace/(3.0*double(NbLoops)));
               
        for(int idxThread = startThread ; idxThread <= endThread ; idxThread *= 2){
               printf(" \t%e \t%e \t%e",
                    totalMergeParallelPermute[idxThread]/(3.0*double(NbLoops)),
                      totalMergeParallelShift[idxThread]/(3.0*double(NbLoops)),
                      totalMergeParallelMulti[idxThread]/(3.0*double(NbLoops)));
        }
        printf("\n");
        
        fflush(stdout);

        delete[] array;
    }
}




////////////////////////////////////////////////////////////////
// Main runs test functions
////////////////////////////////////////////////////////////////

int main(int argc, char** argv){
    if(argc == 2 && strcmp(argv[1],"-reorder") == 0){
        testReorder<int, true>();
        testReorder<int, false>();
    }
    else if(argc == 2 && strcmp(argv[1],"-merge") == 0){
        testMerge<int, true>();
        testMerge<int, false>();
    }
    else if(argc == 2 && strcmp(argv[1],"-time") == 0){
        printf("int\n");
        testMergeTime<int>();
        printf("BlockValue<16>\n");
        testMergeTime<BlockValue<16>>();
        printf("BlockValue<128>\n");
        testMergeTime<BlockValue<128>>();
        printf("BlockValue<512>\n");
        testMergeTime<BlockValue<512>>();
        printf("BlockValue<1024>\n");
        testMergeTime<BlockValue<1024>>();
        printf("BlockValue<16384>\n");
        testMergeTime<BlockValue<16384>>();
    }
    else if(argc == 2 && strcmp(argv[1],"-find") == 0){
        testFindMiddle();
    }
    else{
        printf("[Help] %s [-reorder;-merge;-time;-find]\n", argv[0]);
        return 1;
    }
    return 0;
}
